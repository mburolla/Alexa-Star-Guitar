//
// Name: StarGuitarAlexa.js
// Auth: Martin Burolla
// Date: 7/28/2017
// Desc: The main interface for the Star Guitar Alexa skill.
// Note: https://github.com/alexa/skill-sample-nodejs-trivia/blob/master/src/index.js
//       https://www.youtube.com/watch?v=MRV48SKDyEc
//       https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/speech-synthesis-markup-language-ssml-reference#prosody
//

'use strict';

const Alexa = require('alexa-sdk');
const AlexaScaleEngine = require('./Classes/AlexaScaleEngine');

const APP_ID = 'amzn1.ask.skill.b392e71c-66d8-46b9-ba61-e5478c79bdd1'; 

const handlers = {
    'GetNotesForScale': function () {
        if (!checkSlots(this.event.request.intent)) {
            this.emit(':tellWithCard', 'A name of a valid key and scale is required.');
        } else {
            const alexaScaleEngine = new AlexaScaleEngine();
            const desiredScale = this.event.request.intent.slots.SCALE.value;
            const desiredKey = this.event.request.intent.slots.KEY.value;
            const speechOutput = alexaScaleEngine.getNotes(desiredKey, desiredScale);
            console.log('**** Key: ' + desiredKey + ', ' + 'Scale: ' + desiredScale + '. ****');
            console.log('**** ' + speechOutput + ' ****');
            this.emit(':tellWithCard', speechOutput);
        }
    },
    'GetFormulaForScale': function () {
        if (!checkScaleSlot(this.event.request.intent)) {
            this.emit(':tellWithCard', 'A name of a valid scale is required.');
        } else {
            const alexaScaleEngine = new AlexaScaleEngine();
            const desiredScale = this.event.request.intent.slots.SCALE.value;
            const speechOutput = alexaScaleEngine.getFormula(desiredScale);
            console.log('**** Scale: ' + desiredScale + '. ****');
            console.log('**** ' + speechOutput + ' ****');
            this.emit(':tellWithCard', speechOutput);
        }
    },
    'LaunchRequest': function () { // Executes when a new session is launched.  This is required to pass certifcation.
        const speechOutput = 'What key and scale would you like to know more about?';
        this.emit(':ask', speechOutput); // Ask keeps the session open.
    },
    'KeyScaleIntent': function () {
        if (!checkScaleSlot(this.event.request.intent)) {
            this.emit(':tellWithCard', 'A valid key and scale are required.');
        } else {
            const alexaScaleEngine = new AlexaScaleEngine();
            const desiredKey = this.event.request.intent.slots.KEY.value;
            const desiredScale = this.event.request.intent.slots.SCALE.value;
            const speechOutput = alexaScaleEngine.getNotes(desiredKey, desiredScale) + ' ' +  alexaScaleEngine.getFormula(desiredScale);
            console.log('**** Scale: ' + desiredScale + '. ****');
            console.log('**** ' + speechOutput + ' ****');
            this.emit(':tellWithCard', speechOutput);
        }
    },
    'AMAZON.HelpIntent': function () {
        const speechOutput = 'You can ask me questions like, what are the notes for C minor? Or what is the formula for the major scale?';
        const reprompt = speechOutput;
        this.emit(':ask', speechOutput, reprompt);
    },
    'AMAZON.CancelIntent': function () {
        this.emit(':tell');
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell'); // this.t('STOP_MESSAGE')
    },
};

function checkSlots(intent) {
    let scaleSlot = intent && intent.slots && intent.slots.SCALE && intent.slots.SCALE.value;
    let keySlot =  intent && intent.slots && intent.slots.KEY && intent.slots.KEY.value;
    return keySlot && scaleSlot;
}

function checkScaleSlot(intent) {
    return intent && intent.slots && intent.slots.SCALE && intent.slots.SCALE.value;
}

exports.handler = function (event, context) {
    const alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
};
