//
// Name: ScaleEngine
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc: A pure scale engine with no Alexa validation or speech conversion.
//

const NoteWizard = require('./NoteWizard');
const FormulaManager = require('./FormulaManager');
const NoteTransformer = require('./NoteTransformer');
const CircleOfFifthsHelper = require('./CircleOfFifthsHelper');

//
// Private data members
//

const noteWizard = Symbol();
const formulaManager = Symbol();
const noteTransformer = Symbol();
const circleOfFifthsHelper = Symbol();

module.exports = class ScaleEngine {

  //
  // Constructor
  //

  constructor() {
    this[noteWizard] = new NoteWizard();
    this[formulaManager] = new FormulaManager();
    this[noteTransformer] = new NoteTransformer();
    this[circleOfFifthsHelper] = new CircleOfFifthsHelper();
  }

  //
  // Methods
  //

  findNotes(key, scale) {
    const desiredKey = key.substring(0, 1).toUpperCase() + key.substring(1, 2); 
    const desiredScale = scale.toUpperCase();

    const sequencedNotes = this[noteWizard].scaleSequenceStartingWith(desiredKey);                           // [ 'C', 'D', 'E', 'F', 'G', 'A', 'B' ]
    const transformedNotes = this[noteTransformer].transformNotes(sequencedNotes, desiredScale);             // [ 'C', 'D', 'D#', 'F', 'G', 'G#', 'A#' ]
    const finalNotes = this[circleOfFifthsHelper].formatNotes(transformedNotes, desiredKey, desiredScale);   // [ 'C', 'D', 'Eb', 'F', 'G', 'Ab', 'Bb' ]
    //this.consoleDebug(sequencedNotes, transformedNotes, finalNotes);
    return finalNotes;
  }

  findFormula(scale) {
    this[formulaManager] = new FormulaManager();
    const formula = this[formulaManager].formulaForScale(scale);
    const alexaFormula = this[alexaConverter].convertFormulaToSpeech(formula);   
    return alexaFormula;
  }

  //
  // Helpers
  //

  consoleDebug(seq, tran, final) {
    console.log('***************************************');
    console.log(`Sequenced Notes:   ${seq}`);
    console.log(`Transformed Notes: ${tran}`);
    console.log(`Final Notes:       ${final}`);
    console.log('***************************************');
  }
};
