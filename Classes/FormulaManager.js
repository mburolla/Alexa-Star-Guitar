//
// Name: FormulaManager
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc: Contains all the formulas for all the scales/modes
//

//
// Private data members
//

const formulas = Symbol();

module.exports = class FormulaManager {

  //
  // Constructor
  //

  constructor() {
    this[formulas] = [];
    this[formulas].push({ name: 'MAJOR',            formula: '1,2,3,4,5,6,7'      , numHalfStepsToMajorScale: 0  });
    this[formulas].push({ name: 'IONIAN',           formula: '1,2,3,4,5,6,7'      , numHalfStepsToMajorScale: 0  });
    this[formulas].push({ name: 'DORIAN',           formula: '1,2,3b,4,5,6,7b'    , numHalfStepsToMajorScale: 2  });
    this[formulas].push({ name: 'PHRYGIAN',         formula: '1,2b,3b,4,5,6b,7b'  , numHalfStepsToMajorScale: 4  });
    this[formulas].push({ name: 'LYDIAN',           formula: '1,2,3,4#,5,6,7'     , numHalfStepsToMajorScale: 5  });
    this[formulas].push({ name: 'MIXOLYDIAN',       formula: '1,2,3,4,5,6,7b'     , numHalfStepsToMajorScale: 7  });
    this[formulas].push({ name: 'MINOR',            formula: '1,2,3b,4,5,6b,7b'   , numHalfStepsToMajorScale: 9  });
    this[formulas].push({ name: 'AEOLIAN',          formula: '1,2,3b,4,5,6b,7b'   , numHalfStepsToMajorScale: 9  });
    this[formulas].push({ name: 'MINOR PENTATONIC', formula: '1,3b,4,5,7b'        , numHalfStepsToMajorScale: 9  }); 
    this[formulas].push({ name: 'MINOR BLUES',      formula: '1,3b,4,5b,5,7b'     , numHalfStepsToMajorScale: 9  }); 
    this[formulas].push({ name: 'LOCRIAN',          formula: '1,2b,3b,4,5b,6b,7b' , numHalfStepsToMajorScale: 11 });
  }

  //
  // Methods
  //
  
  formulaForScale(scale) {   
    scale = scale.toUpperCase();
    for (let i = 0; i < this[formulas].length; i++) {
       if (scale === this[formulas][i].name) {
         return this[formulas][i].formula.split(',');
       }
    }
  }

  formulas() {
    return this[formulas].slice(0);
  }
};
