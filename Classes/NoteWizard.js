//
// Name: NoteWizard
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc: Nothing can kill the NoteWizard.
//

//
// Private data members
//

const referenceNotes = Symbol();
const preferFlat = Symbol();

module.exports = class NoteWizard {

    //
    // Constructor
    //

    constructor() { 
        this[referenceNotes] = [];
        this[referenceNotes].push({ note: 'A',  alias: ''   });
        this[referenceNotes].push({ note: 'A#', alias: 'Bb' });
        this[referenceNotes].push({ note: 'B',  alias: ''   });
        this[referenceNotes].push({ note: 'C',  alias: ''   });
        this[referenceNotes].push({ note: 'C#', alias: 'Db' });
        this[referenceNotes].push({ note: 'D',  alias: ''   });
        this[referenceNotes].push({ note: 'D#', alias: 'Eb' });
        this[referenceNotes].push({ note: 'E',  alias: ''   });
        this[referenceNotes].push({ note: 'F',  alias: ''   });
        this[referenceNotes].push({ note: 'F#', alias: 'Gb' });
        this[referenceNotes].push({ note: 'G',  alias: ''   });
        this[referenceNotes].push({ note: 'G#', alias: 'Ab' });
    }

    //
    // Methods
    //

    scaleSequenceStartingWith(note) {
        this[preferFlat] = false;
        if (note.search('b') === 1) {
            this[preferFlat] = true;
        }

        // Build Scale Sequence: WWH-W-WWH (minus last half step).
        const retval = [];
        retval.push(note);
        retval.push(this.nextWholeNoteFromThisNote(retval[0]));
        retval.push(this.nextWholeNoteFromThisNote(retval[1]));
        retval.push(this.nextHalfNoteFromThisNote(retval[2]));
        retval.push(this.nextWholeNoteFromThisNote(retval[3]));
        retval.push(this.nextWholeNoteFromThisNote(retval[4]));
        retval.push(this.nextWholeNoteFromThisNote(retval[5]));
        return retval;
    }

    nextWholeNoteFromThisNote(note) {
        let foundIndex = this.noteIndex(note);
        foundIndex += 2;
        if (foundIndex > 11) {
            foundIndex -= 12;
        }
        return this.returnNote(foundIndex);
    }

    nextHalfNoteFromThisNote(note) {
        let foundIndex = this.noteIndex(note);
        foundIndex += 1;
        if (foundIndex > 11) {
            foundIndex -= 12;
        }
        return this.returnNote(foundIndex);
    }

    prevWholeNoteFromThisNote(note) {
        let foundIndex = this.noteIndex(note);
        foundIndex -= 2;
        if (foundIndex < 0) {
            foundIndex += 12;
        }
        return this.returnNote(foundIndex);
    }

    prevHalfNoteFromThisNote(note) {
        let foundIndex = this.noteIndex(note);
        foundIndex -= 1;
        if (foundIndex < 0) {
            foundIndex += 12;
        }
        return this.returnNote(foundIndex);
    }

    //
    // Helpers
    //

    noteIndex(note) {
        let retval = 0;
        this[referenceNotes].forEach((item, index) => {
            if (item.note === note || item.alias === note) {
                retval = index;
            }
        });
        return retval;
    }

    returnNote(index) {
        let note = this[referenceNotes][index];
        if (note.alias.length > 0 && this[preferFlat] === true) {
            return note.alias;
        } else {
            return note.note;
        }
    }
};
