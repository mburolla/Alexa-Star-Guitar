//
// Name: NoteTransformer
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc: 
//

const FormulaManager = require('./FormulaManager');
const NoteWizard = require('./NoteWizard');

//
// Private data members
//

const formulaManager = Symbol();
const noteWizard = Symbol();

const NoteType = {
    Natural: 0,
    Sharp: 1,
    Flat: 2
};

module.exports = class NoteTransformer {

    //
    // Constructor
    //

    constructor() {
        this[formulaManager] = new FormulaManager();
        this[noteWizard] = new NoteWizard();
    }

    //
    // Note transformations
    //

    transformNotes(notes, scale) { // [ 'C', 'D', 'E', 'F', 'G', 'A', 'B' ] ==> [ 'C', 'D', 'D#', 'F', 'G', 'G#', 'A#' ]
        const retval = [];
        const formula = this[formulaManager].formulaForScale(scale); 
       
        formula.forEach((noteNumberInFormula) => {
            const noteType = this.determineNoteType(noteNumberInFormula);
            const unadjustedNote = this.noteAtPosition(notes, noteNumberInFormula);  
         
            if (noteType === NoteType.Natural) {
                retval.push(unadjustedNote);
            } 
            if (noteType === NoteType.Sharp) {
                retval.push(this[noteWizard].nextHalfNoteFromThisNote(unadjustedNote));
            } 
            if (noteType === NoteType.Flat) {
                retval.push(this[noteWizard].prevHalfNoteFromThisNote(unadjustedNote));
            } 
        });
        return retval;
    }

    determineNoteType(noteNumberInFormula) {
        let retval = NoteType.Natural;
        if (noteNumberInFormula.search('#') === 1) {
            retval = NoteType.Sharp;
        } 
        if (noteNumberInFormula.search('b') === 1) {
            retval = NoteType.Flat;
        } 
        return retval;
    }

    noteAtPosition(notes, noteNumber) {
        return notes[noteNumber.substring(0, 1) - 1];
    }
};
