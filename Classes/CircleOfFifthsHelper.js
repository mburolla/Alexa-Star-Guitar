//
// Name: CircleOfFifthsHelper
// Auth: Martin Burolla
// Date: 6/29/2017
// Desc: 
//

const NoteWizard = require('./NoteWizard');
const FormulaManager = require('./FormulaManager');

//
// Data members
//

const circleOfFifthsLookUp = Symbol();
const parentScaleLookUp = Symbol();
const noteWizard = Symbol();
const formulaManager = Symbol();

module.exports = class CircleOfFifthsHelper {

    constructor() {
        this[noteWizard] = new NoteWizard();
        this[formulaManager] = new FormulaManager();

        this[circleOfFifthsLookUp] = [];
        this[circleOfFifthsLookUp].push({ key: 'C',  displayFormat: 'Natural' });
        this[circleOfFifthsLookUp].push({ key: 'G',  displayFormat: 'Sharp'   });
        this[circleOfFifthsLookUp].push({ key: 'D',  displayFormat: 'Sharp'   });
        this[circleOfFifthsLookUp].push({ key: 'A',  displayFormat: 'Sharp'   });
        this[circleOfFifthsLookUp].push({ key: 'E',  displayFormat: 'Sharp'   });
        this[circleOfFifthsLookUp].push({ key: 'B',  displayFormat: 'Sharp'   });
        this[circleOfFifthsLookUp].push({ key: 'F#', displayFormat: 'Sharp'   });
        this[circleOfFifthsLookUp].push({ key: 'Gb', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'Db', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'Ab', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'Eb', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'Bb', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'F',  displayFormat: 'Flat'    });
        //
        this[circleOfFifthsLookUp].push({ key: 'A#', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'C#', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'D#', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'F#', displayFormat: 'Flat'    });
        this[circleOfFifthsLookUp].push({ key: 'G#', displayFormat: 'Flat'    });
    }

    formatNotes(notes, key, mode) {
        return this.adjustTheNotes(notes, this.determineDesiredDisplayFormat(key, mode));
    }

    adjustTheNotes(notes, desiredDisplayFormat) {
        const retval = [];
        notes.forEach((item) => {
            if (item.length === 1) {
                retval.push(item);
            } else if (item.search('#') === 1 && desiredDisplayFormat === 'Flat') {
                let newNote = this[noteWizard].nextHalfNoteFromThisNote(item);
                newNote += 'b';
                retval.push(newNote);
            } else if (item.search('b') === 1 && desiredDisplayFormat === 'Sharp') {
                let newNote = this[noteWizard].prevHalfNoteFromThisNote(item);
                newNote += '#';
                retval.push(newNote);
            } else if (item.search('#') === 1 && desiredDisplayFormat === 'Sharp') {
                retval.push(item); // Just push it.
            } else if (item.search('b') === 1 && desiredDisplayFormat === 'Flat') {
                retval.push(item); // Push it good (real good).
            }
        });
        return retval;
    }

    determineDesiredDisplayFormat(key, mode) {
        mode = mode.toUpperCase();
        let retval = 'Natural';

        if (key.length > 1) { 
            if (key.substring(1, 2) === '#') {
                retval = 'Sharp';
            }
            if (key.substring(1, 2) === 'b') {
                retval = 'Flat';
            }
        } else {
            const parentKey = this.parentKeyForKeyAndMode(key, mode);
            this[circleOfFifthsLookUp].forEach((item) => {
                if (item.key === parentKey) {
                    retval = item.displayFormat;
                }
            });
            //console.log(`Parent key: ${parentKey}, Desired format: ${retval}`);
        }
        return retval;
    }

    parentKeyForKeyAndMode(key, mode) {
        mode = mode.toUpperCase();
        let retval = key;
        let numHalfSteps = 0;
        this[formulaManager].formulas().forEach((item) => {
            if (item.name === mode) {
                numHalfSteps = item.numHalfStepsToMajorScale;
            }
        });
        for (let i = 0; i < numHalfSteps; i++) {
            retval = this[noteWizard].prevHalfNoteFromThisNote(retval);
        }
        return retval;
    }
};
