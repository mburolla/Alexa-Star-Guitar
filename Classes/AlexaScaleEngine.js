//
// Name: AlexaScaleEngine
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc: Provides a safe interface around the ScaleEngine cleaning any input and 
//       output for Alexa.
//

const ScaleEngine = require('./ScaleEngine');
const FormulaManager = require('./FormulaManager');

//
// Private data members
//

const scaleEngine = Symbol();
const formulaManager = Symbol();

module.exports = class AlexaScaleEngine {

    constructor() {
        this[scaleEngine] = new ScaleEngine();
        this[formulaManager] = new FormulaManager();
        this.validKeys = ['A', 'A#', 'Bb', 'B', 'C', 'C#', 'Db', 'D', 'D#', 'Eb', 'E', 'F', 'F#', 'Gb', 'G', 'G#', 'Ab'];
    }

    getNotes(key, scale) {
        let fixedKey = this.keyFixerUpper(key);
        let keyValidation = this.validateKey(fixedKey);
        let scaleValidation = this.validateScale(scale);

        if (!keyValidation.isValid) {
            return keyValidation.message;
        }

        if (!scaleValidation.isValid) {
            return scaleValidation.message;
        }

        // We must be good to go!
        let retval = `<s>The notes for ${key.replace('.', '')} ${scale} are: `;
        retval += (this.convertNotesToSpeech(this[scaleEngine].findNotes(fixedKey, scale)) + '.</s>');
        return retval;   
    }

    getFormula(scale) {
        let scaleValidation = this.validateScale(scale);
        if (!scaleValidation.isValid) {
            return scaleValidation.message;
        }

        let retval = `<s>The formula for the ${scale} scale is: `;
        const formula = this[formulaManager].formulaForScale(scale.toUpperCase());
        retval += (this.convertFormulaToSpeech(formula) + '.</s>');
        return retval;
    }

    //
    // Input scrubbing
    //

    keyFixerUpper(key) { 
        let retval = key.toUpperCase();
        retval = retval.replace('.', '');  // Alexa, why you do this?
        if (retval.search('SHARP') >= 0) {
            retval = retval.substring(0, 1) + "#";
        }
        if (retval.search('FLAT') >= 0) {
            retval = retval.substring(0, 1) + "b";
        }
        return retval;
    }

    //
    // Validation
    //

    validateKey(key) {
        let foundKey = false;
        if (key === "Cb" || key === "B#" || key === "E#" || key === "Fb") {
            return { isValid: false, message: 'There is no such key.'};
        }
        this.validKeys.forEach((item) => {
            if (item === key) {
                foundKey = true;
            }
        });

        if (foundKey) {
            return { isValid: true, message: ''};
        } else {
            return { isValid: false, message: `I didn't understand that key.`};
        }
    }

    validateScale(scale) {
        let formula = this[formulaManager].formulaForScale(scale);
        if (formula === undefined) {
            return { isValid: false, message: `I don't know that scale yet but I will learn it.`};
        } else {
            return { isValid: true, message: ''};
        }
    }

    //
    // Output scrubbing
    //

    convertNotesToSpeech(notes) {
        let retval = '';
        if (notes.length < 5) { // Dirty sanchez!
            return 'Oops, something went wrong.'
        } else {
            notes.forEach((item) => {
                if (item.length === 1) {
                    retval += item;
                    retval += ', ';
                } else if (item.search('b') === 1) {
                    retval += item.substring(0, 1);
                    retval += ' Flat, ';
                } else if (item.search('#') === 1) {
                    retval += item.substring(0, 1);
                    retval += ' Sharp, ';
                }
            });
            return retval.substring(0, retval.length - 2); // Lop off last comma.
        }
    }

    convertFormulaToSpeech(notes) {
        let retval = '';
        notes.forEach((item) => {
            if (item.length === 1) {
                retval += item;
                retval += ', ';
            } else if (item.search('b') === 1) {
                retval += 'Flat ';
                retval += item.substring(0, 1);
                retval += ', ';
            } else if (item.search('#') === 1) {
                retval += 'Sharp ';
                retval += item.substring(0, 1);
                retval += ', ';
            }
        });
        return retval.substring(0, retval.length - 2); // Lop off last comma.
    }
};

// validateInput(key) {
//     if (key === "Cb" || key === "B#" || key === "E#" || key === "Fb") {
//         return { errors: true, message: 'There is no such key.'};
//     } else {
//         return { errors: false, message: ''};
//     }
// }
