//
// Name: playAudio.js
// Auth: Martin Burolla
// Date: 7/6/2017
//

'use strict';

const Alexa = require('alexa-sdk');
const APP_ID = 'amzn1.ask.skill.0551021e-b2dd-40de-bf5e-9b34da3b27a8'; 

const handlers = {
    'LaunchRequest': function () { 
        this.response.audioPlayerPlay('REPLACE_ALL', 'https://s3.amazonaws.com/starguitar.music/VanHalen.mp3', 123, null, 0);
        this.emit(':responseReady');
    },
    'TestIntent': function () { 
        this.emit(':tellWithCard', 'This is a test.');
    },
    'PlayVanHalenIntent': function() {
        this.response.audioPlayerPlay('REPLACE_ALL', 'https://s3.amazonaws.com/starguitar.music/VanHalen.mp3', 123, null, 0);
        this.emit(':responseReady');
    },
    'StopAudioIntent': function() {
        this.response.audioPlayerStop();
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function () {
        const speechOutput = 'You can tell me to play some audio.';
        const reprompt = speechOutput;
        this.emit(':ask', speechOutput, reprompt);
    },
    'AMAZON.CancelIntent': function () {
        this.response.audioPlayerStop();
        this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function () {
        this.response.audioPlayerStop();
        this.emit(':responseReady');
    },
};

exports.handler = function (event, context) {
    const alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
};

/*
{
  "intents": [
    {
      "intent": "AMAZON.PauseIntent"
    },
    {
      "intent": "AMAZON.ResumeIntent"
    },
    {
      "intent": "TestIntent"
    },
    {
      "intent": "PlayVanHalenIntent"
    },
    {
      "intent": "StopAudioIntent"
    }
  ]
}

TestIntent test
PlayVanHalenIntent play van halen
StopAudioIntent stop

 */