# Star Guitar Alexa Skill
An Alexa skill that tells the notes for a given key and scale.  For example: Alexa... What are the notes for C Minor?

# OOP JavaScript
Can JavaScript be object oriented like C#, Java and C++?  You decide!  This project was an excerise in OOP using the object oriented constructs available in ES6.  OOP in JavaScript certainly looks a bit clunky when compared to real OOP languages like C# and Java.  


# How Does it Work?
A scale engine is loaded with the formulas for music scales during the intial load of the application.  Currently this skill only supports the popular scales/modes, but in theory can support any scale.

#### Supported Scales

* Major
* Ionian
* Dorian
* Phrygian
* Lydian
* Mixolydian 
* Aeolian
* Minor
* Locrian
* Minor Blues
* Minor Pentatonic

# Fun Fact
Amazon provides me with a 100 credit every month against my AWS bill because of this skill.  Want a free cloud?  Create an Alexa skill!

