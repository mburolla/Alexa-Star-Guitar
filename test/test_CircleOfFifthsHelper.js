//
// Name: test_CircleOfFifthsHelper
// Auth: Martin Burolla
// Date: 6/29/2017
// Desc:
//

const CircleOfFifthsHelper = require('../Classes/CircleOfFifthsHelper');

const cofh = new CircleOfFifthsHelper();

//console.log(cofh.parentKeyForKeyAndMode('C', 'Lydian'));

const notes = [];
notes.push('C');
notes.push('D');
notes.push('D#');
notes.push('F');
notes.push('G');
notes.push('G#');
notes.push('A#');

// notes.push('C');
// notes.push('D');
// notes.push('E');
// notes.push('F');
// notes.push('G');
// notes.push('A');
// notes.push('B');

const fm = cofh.formatNotes(notes, 'C', 'Minor');

console.log(fm);

