//
// Name: test_ScaleEngine
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc:
//

const AlexaScaleEngine = require('../Classes/AlexaScaleEngine');
const FormulaManager = require('../Classes/FormulaManager');

const alexaScaleEngine = new AlexaScaleEngine();
const formulaManager = new FormulaManager();

const keys = ['a', 'a sharp', 'b flat', 'b', 'c', 'c sharp', 'd flat', 'd', 'd sharp', 'e flat', 'e', 'f', 'f sharp', 'g flat', 'g', 'g sharp', 'a flat'];

console.log('********************************************************************');
keys.forEach((key) => {
    formulaManager.formulas().forEach((scale) => {
        console.log(`${key.toUpperCase()} ${scale.name} ==> ${alexaScaleEngine.getNotes(key, scale.name)}`);
    });
    console.log('');
});

console.log('****');
console.log('C FLAT: ' + alexaScaleEngine.getNotes('c flat', 'major'));
console.log('B SHARP: ' + alexaScaleEngine.getNotes('b sharp', 'major'));
console.log('F FLAT: ' + alexaScaleEngine.getNotes('f flat', 'major'));
console.log('E SHARP: ' + alexaScaleEngine.getNotes('e sharp', 'major'));
console.log('****');
console.log('G MAJOR: ' + alexaScaleEngine.getNotes('g', 'major'));
console.log('FORMULA FOR MINOR SCALE: ' + alexaScaleEngine.getFormula('minor'));
console.log('****');
console.log('BIFF TEST 0: ' + alexaScaleEngine.getNotes('', ''));
console.log('BIFF TEST 1: ' + alexaScaleEngine.getNotes('g. sharp', 'major'));
console.log('BIFF TEST 2: ' + alexaScaleEngine.getNotes('b.', 'major'));
console.log('BIFF TEST 3: ' + alexaScaleEngine.getNotes('go to shark', 'major'));
console.log('BIFF TEST 4: ' + alexaScaleEngine.getNotes('E', 'may-jor'));
console.log('BIFF TEST 5: ' + alexaScaleEngine.getNotes('F', 'Fart scale'));
console.log('BIFF TEST 6: ' + alexaScaleEngine.getFormula('butt scale'));
