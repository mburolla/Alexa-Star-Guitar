//
// Name: test_FormulaManager
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc:
//

const FormulaManager = require('../Classes/FormulaManager');

const fm = new FormulaManager();

console.log(fm.formulaForScale('Minor'));
console.log(fm.formulaForScale('Major'));
