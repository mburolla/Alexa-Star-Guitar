//
// Name: test_ScaleEngine
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc:
//

const ScaleEngine = require('../Classes/ScaleEngine');
const FormulaManager = require('../Classes/FormulaManager');

const scaleEngine = new ScaleEngine();
const formulaManager = new FormulaManager();
const keys = ['A', 'A#', 'Bb', 'B', 'C', 'C#', 'Db', 'D', 'D#', 'Eb', 'E', 'F', 'F#', 'Gb', 'G', 'G#', 'Ab'];

//
// TEST THEM ALL
//

// console.log('********************************************************************');
// keys.forEach((key) => {
//     formulaManager.formulas().forEach((scale) => {
//         console.log(`${key} ${scale.name} ==> ${scaleEngine.findNotes(key, scale.name)}`);
//     });
//     console.log('');
// });


//
// TEST ALL SCALES IN A KEY
//

// const key = 'A#';
// formulaManager.formulas().forEach((scale) => {
//     console.log(`${key} ${scale.name} ==> ${scaleEngine.findNotes(key, scale.name)}`);
// });
// console.log('');



//
// TEST ONE KEY AND ONE SCALE
//

const key = 'g#';
const scale = 'major';
const notes = scaleEngine.findNotes(key, scale); 
console.log(notes);
