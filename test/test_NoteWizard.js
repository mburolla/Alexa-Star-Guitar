//
// Name: test_NoteKeeper
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc:
//

const NoteWizard = require('../Classes/NoteWizard');

const noteWizard = new NoteWizard;

// console.log(noteWizard.scaleSequenceStartingWith('Gb'));
// console.log(noteWizard.scaleSequenceStartingWith('F#'));

// console.log(noteWizard.nextWholeNoteFromThisNote('G#'));
// console.log(noteWizard.nextWholeNoteFromThisNote('Ab'));

// console.log(noteWizard.nextHalfNoteFromThisNote('G#'));
// console.log(noteWizard.prevWholeNoteFromThisNote('A'));
// console.log(noteWizard.prevHalfNoteFromThisNote('C'));

// console.log(noteWizard.prevHalfNoteFromThisNote('Db'));

 console.log(noteWizard.scaleSequenceStartingWith('Eb'));