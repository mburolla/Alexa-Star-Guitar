//
// Name: test_NoteTransformer
// Auth: Martin Burolla
// Date: 6/28/2017
// Desc:
//

const NoteTransformer = require('../Classes/NoteTransformer');
const NoteWizard = require('../Classes/NoteWizard');

const noteWizard = new NoteWizard();
const noteTransformer = new NoteTransformer();

const notes = noteWizard.scaleSequenceStartingWith('C');
console.log(notes);
const transformedNotes = noteTransformer.transformNotes(notes, 'Minor');
console.log(transformedNotes);
